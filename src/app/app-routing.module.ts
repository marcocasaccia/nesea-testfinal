import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './components/products/products.component';
import { ProductComponent } from './components/product/product.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { CategoryComponent } from './components/category/category.component';


const routes: Routes = [{
    component: ProductsComponent,
    path: 'app-products/id'
},
{
    component: ProductComponent,
    path: 'app-product'
}, {
    component: CategoriesComponent,
    path: 'app-categories'
},
{
    component: CategoryComponent,
    path: 'app-category'
},
{
    path: '',
    pathMatch: 'full',
    redirectTo: 'app-products'
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
