import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { IProduct } from '../interfaces/product.interface';
import { Product } from '../models/product.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private _http: HttpClient) { }

  public listProduct(category_id): Observable<Product[]> {

    let url = `${environment.api.url}/products`;

    if (category_id) {
      url += `/?category_id=${category_id}`;
    }

    return this._http.get<IProduct[]>(`${(environment.api.url)}/products`).pipe(
      map(e => {
        const products: Product[] = [];

        for (const prod of e) {
          products.push(new Product(prod.id, prod.name, prod.description, prod.image, prod.price, prod.category_id));
        }

        return products;
      })
    );
  }
}

// this.http è un'istanza del servizio HttpClient. Espone una serie di metodi che hanno per nome i verbi HTTP.
// Il metodo get infatti permette di effettuare una chiamata all'indirizzo specificato tra parentesi tonde: il tipo di dato restituito
// invece viene descritto tra parentesi angolari
//
// get, come tutti i metodi di httpclient, restituiscono un observable: possiamo lavorare con gli elementi restituiti applicando degli
// operators, da descrivere in ordine all'interno del metodo pipe
//
// Gli operators permettono di ricevere i dati emessi e lavorarli, riemettendoli su uno stream parallelo all'originale senza alterarne
// il contenuto: in questo modo, altri eventuali ascoltatori non vedranno i dati alterati
//
// map permette di applicare una callback a ciascun elemento emesso dall'observable: quando get riceverà una risposta dall'api, questa
// consisterà in un unico elemento di tipo array. Quando map restituirà un elemento con return, questo verrà riemesso all'operator successivo