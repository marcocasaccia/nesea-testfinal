import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { ICategory } from '../interfaces/category.interface';
import { Category } from '../models/category.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private _http: HttpClient) { }

  public listCat(): Observable<Category[]> {

    return this._http.get<ICategory[]>(`${(environment.api.url)}/categories`).pipe(
      map(e => {
        const categories: Category[] = [];

        for (const c of e) {
          categories.push(new Category(c.id, c.name));
        }

        return categories;
      })
    );
  }
}


// this.http è un'istanza del serzio HttpClient. Espone una serie di metodi che hanno per nomi i verbi HTTP.
// il metodo get infatti permette di effettuare una chiamata all'indirizzo  specificato tra parentesi tonde:
// il tipo di dato restituito invece  viene descritto tra parentesi angolari  