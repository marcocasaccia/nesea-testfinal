import { Component, OnInit } from '@angular/core';
import { Category } from '../../models/category.model';
import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public categories: Category[];

  constructor(private _categoryService: CategoryService) {
    this.categories = [];
  }

  ngOnInit() {
    this._categoryService.listCat().subscribe(categoryServ => this.categories = categoryServ);
  }

}
