import { Component, OnInit } from '@angular/core';
import { Category } from '../../models/category.model';
import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  public categories: Category[];

  constructor(private _categoryService: CategoryService) {
    this.categories = [];
  }

  ngOnInit() {
    this._categoryService.listCat().subscribe(categoryServ => this.categories = categoryServ);
  }

}
